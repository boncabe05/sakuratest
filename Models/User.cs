﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sakuratest.Models
{
    public partial class User
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
    }
}
