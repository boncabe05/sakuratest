﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sakuratest.Models
{
    public class OrderList
    {
        [Key]
        public string OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public string CustomerName { get; set; }
        public string ShipName { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
