﻿using System;
using System.Collections.Generic;

namespace sakuratest.Models
{
    public partial class Orderheader
    {
        public string OrderId { get; set; }
        public DateTime? OrderDate { get; set; }
        public string CustomerId { get; set; }
        public DateTime? RequiredDate { get; set; }
        public string ShipName { get; set; }
        public decimal? TotalPrice { get; set; }
    }
}
