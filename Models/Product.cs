﻿using System;
using System.Collections.Generic;

namespace sakuratest.Models
{
    public partial class Product
    {
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal? Price { get; set; }
    }
}
