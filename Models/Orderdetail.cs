﻿using System;
using System.Collections.Generic;

namespace sakuratest.Models
{
    public partial class Orderdetail
    {
        public string OrderId { get; set; }
        public string ProductId { get; set; }
        public int? Qty { get; set; }
        public decimal? Subtotal { get; set; }
    }
}
