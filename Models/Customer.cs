﻿using System;
using System.Collections.Generic;

namespace sakuratest.Models
{
    public partial class Customer
    {
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
    }
}
