using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using sakuratest.Models;

namespace sakura_test.Controllers
{
    public class LoginController : Controller
    {
        private readonly SalesDbContext _context;

        public LoginController(SalesDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index([Bind("UserName", "Password")] User user)
        {
            if(ModelState.IsValid){
                var _user = new SqlParameter("@username", user.UserName.ToLower());
                var _pass = new SqlParameter("@password", user.Password);
                var result = await _context.User.FromSqlRaw(@"Exec dbo.Login_User @username , @password", _user, _pass).ToListAsync();
                int res = result.Count;
                if(res == 0){
                    ModelState.AddModelError("incorrect", "The username or password is incorrect. ");
                }else{
                    var isAdmin = result.Select(x => x.IsAdmin).FirstOrDefault();
                    var user_ = result.Select(x => x.UserName).FirstOrDefault();
                    HttpContext.Session.SetString("isAdmin", isAdmin.ToString().ToLower());
                    HttpContext.Session.SetString("username", user_);
                    return RedirectToAction("Index", "Home");
                }
            }
            return View(user);
        }

        [Route("logout")]
        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.Session.Remove("username");
            HttpContext.Session.Remove("isAdmin");
            return RedirectToAction("Index", "Home");
        }
    }
}
