﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using sakuratest.Models;

namespace sakura_test.Controllers
{
    public class OrderListController : Controller
    {
        private readonly SalesDbContext _context;

        public OrderListController(SalesDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Index(OrderList _orderList)
        {
            var _filter_order_no = new SqlParameter("@Filter_OrderNo_", 0);
            var _filter_customer = new SqlParameter("@Filter_Customer_", 0);
            var _orderId = new SqlParameter("@OrderID_", "");
            var _customerId = new SqlParameter("@CustomerID_", "");
            var orderList = await _context.OrderList.FromSqlRaw("Exec dbo.OrderList @Filter_OrderNo_ , @Filter_Customer_, @OrderID_, @CustomerID_", _filter_order_no, _filter_customer, _orderId, _customerId).ToListAsync();
            return View(orderList);
        }

    }
}
